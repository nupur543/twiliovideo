//
//  TVICameraSourceOptions.h
//  TwilioVideo
//
//  Copyright © 2018 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import "TVIVideoFrame.h"

/**
 *  `TVICameraSourceOptionsBuilder` is a builder class for `TVICameraSourceOptions`.
 */
@interface TVICameraSourceOptionsBuilder : NSObject

/**
 *  @brief Allows you to override how frames produced by `TVICameraSource` are tagged. Defaults to `NO`.
 *
 *  @discussion This method is for advanced use cases. By default `TVICameraSource` derives orientation tags based upon
 *  listening to changes in the status bar's orientation. However, you may wish to listen for `UIDeviceOrientation`
 *  or some other input source instead.
 *
 *  Note that the video data output connection will always be configured for the native orientation of the camera sensor.
 *  The connection is set to `AVCaptureVideoOrientationLandscapeLeft` for front facing devices, and
 *  `AVCaptureVideoOrientationLandscapeRight` for rear facing devices. For example, if you are holding your iOS device in
 *  `AVCaptureVideoOrientationLandscapeLeft` (`UIDeviceOrientationLandscapeRight`) with the front
 *  camera facing towards you, then no rotation is required and the correct tag would be `TVIVideoOrientationUp`.
 */
@property (nonatomic, assign) BOOL enableManualOrientation;

/**
 *  Enable the use of `TVICameraPreviewView` to preview video from the camera. Defaults to `NO`.
 */
@property (nonatomic, assign) BOOL enablePreview;

/**
 *  @brief The initial orientation tag to use. Only valid when `enableManualOrientation` is set to `YES`.
 *  Defaults to `TVIVideoOrientationUp`.
 *
 *  @discussion If you've already determined the orientation you would like to tag frames with, then you can set it here.
 *  This ensures that the first frame produced by the source will have the tag that you expect.
 */
@property (nonatomic, assign) TVIVideoOrientation orientation;

/**
 *  @brief The initial zoom factor to use for the capture device. Throws an `NSInvalidArgumentException` if the
 *  selected zoom factor is not supported by the device. Defaults to `1.0`.
 *
 *  @see [AVCaptureDevice](AVCaptureDevice.h)
 */
@property (nonatomic, assign) CGFloat zoomFactor;

/**
 *  @brief You should not initialize `TVICameraSourceOptionsBuilder` directly, use a `TVICameraSourceOptionsBuilderBlock` instead.
 */
- (null_unspecified instancetype)init __attribute__((unavailable("Use a TVICameraSourceOptionsBuilderBlock instead.")));

@end

/**
 *  `TVICameraSourceOptionsBuilderBlock` allows you to construct `TVICameraSourceOptions` using the builder pattern.
 *
 *  @param builder The builder.
 */
typedef void (^TVICameraSourceOptionsBuilderBlock)(TVICameraSourceOptionsBuilder * _Nonnull builder);

/**
 *  Represents immutable configuration options for a `TVICameraSource`.
 */
@interface TVICameraSourceOptions : NSObject

/**
 *  @brief Allows you to override how frames produced by `TVICameraSource` are tagged. Defaults to `NO`.
 *
 *  @discussion This method is for advanced use cases. By default `TVICameraSource` derives orientation tags based upon
 *  listening to changes in the status bar's orientation. However, you may wish to listen for `UIDeviceOrientation`
 *  or some other input source instead.
 *
 *  Note that the video data output connection will always be configured for the native orientation of the camera sensor.
 *  The connection is set to `AVCaptureVideoOrientationLandscapeLeft` for front facing devices, and
 *  `AVCaptureVideoOrientationLandscapeRight` for rear facing devices. For example, if you are holding your iOS device in
 *  `AVCaptureVideoOrientationLandscapeLeft` (`UIDeviceOrientationLandscapeRight`) with the front
 *  camera facing towards you, then no rotation is required and the correct tag would be `TVIVideoOrientationUp`.
 */
@property (nonatomic, assign, readonly) BOOL enableManualOrientation;

/**
 *  Enable the use of `TVICameraPreviewView` to preview video from the camera. Defaults to `NO`.
 */
@property (nonatomic, assign, readonly) BOOL enablePreview;

/**
 *  @brief The initial orientation tag to use. Only valid when `enableManualOrientation` is set to `YES`.
 *  Defaults to `TVIVideoOrientationUp`.
 *
 *  @discussion If you've already determined the orientation you would like to tag frames with, then you can set it here.
 *  This ensures that the first frame produced by the source will have the tag that you expect.
 */
@property (nonatomic, assign, readonly) TVIVideoOrientation orientation;

/**
 *  @brief The initial zoom factor to use for the capture device. Throws an `NSInvalidArgumentException` if the
 *  selected zoom factor is not supported by the device. Defaults to `1.0`.
 *
 *  @see [AVCaptureDevice](AVCaptureDevice.h)
 */
@property (nonatomic, assign, readonly) CGFloat zoomFactor;

/**
 *  @brief Developers shouldn't initialize this class directly.
 *
 *  @discussion Use the class method `optionsWithBlock:` instead.
 */
- (null_unspecified instancetype)init __attribute__((unavailable("Use optionsWithBlock: to create a TVICameraSourceOptions instance.")));

/**
 *  @brief Creates an instance of `TVICameraSourceOptions` using a builder block.
 *
 *  @param block The builder block which will be used to configure the `TVICameraSourceOptions` instance.
 *
 *  @return An instance of `TVICameraSourceOptions`.
 */
+ (nonnull instancetype)optionsWithBlock:(nonnull TVICameraSourceOptionsBuilderBlock)block;

@end
