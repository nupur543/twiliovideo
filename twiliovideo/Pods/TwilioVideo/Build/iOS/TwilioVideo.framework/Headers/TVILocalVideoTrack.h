//
//  TVILocalVideoTrack.h
//  TwilioVideo
//
//  Copyright © 2017 Twilio, Inc. All rights reserved.
//

#import "TVIVideoTrack.h"

@class TVIVideoConstraints;
@protocol TVIVideoCapturer;
@protocol TVIVideoSource;

/**
 * `TVILocalVideoTrack` represents local video produced by a `TVIVideoCapturer`.
 */
@interface TVILocalVideoTrack : TVIVideoTrack

/**
 *  @brief Indicates if track is enabled.
 *
 *  @discussion It is possible to enable and disable local tracks. The results of this operation are signaled to other
 *  Participants in the same Room. When a video track is disabled, black frames are sent in place of normal video.
 */
@property (nonatomic, assign, getter = isEnabled) BOOL enabled;

/**
 *  @brief The capturer that is associated with this track when using the `TVIVideoCapturer` based initializers.
 *
 *  @discussion With the addition of the `TVIVideoSource` based API, this property would need to be `nullable`, however
 *  making that change would break SemVer compliance and would cause current Swift based appications to crash if accessing
 *  this property when a `TVILocalVideoTrack` is created with a `TVIVideoSource` object. Therefore, if a `TVILocalVideoTrack`
 *  is created using a `TVIVideoSource` object, accessing this property will return a placeholder object to maintain
 *  SemVer compliance.
 */
@property (nonatomic, strong, readonly, nonnull) id<TVIVideoCapturer> capturer
DEPRECATED_MSG_ATTRIBUTE("TVIVideoCapturer is deprecated. Use TVIVideoSource instead.");

/**
 *  @brief The video constraints that are associated with this track when using the `TVIVideoCapturer` based initializers.
 *
 *  @discussion With the addition of the `TVIVideoSource` based API, this property would need to be `nullable`, however
 *  making that change would break SemVer compliance and would cause current Swift based appications to crash if accessing
 *  this property when a `TVILocalVideoTrack` is created with a `TVIVideoSource` object. Therefore, if a `TVILocalVideoTrack`
 *  is created using a `TVIVideoSource` object, accessing this property will return a `TVIVideoConstraints` object
 *  with the default values to maintain SemVer compliance.
 */
@property (nonatomic, strong, readonly, nonnull) TVIVideoConstraints *constraints
DEPRECATED_MSG_ATTRIBUTE("Constraints are deprecated. You should use TVIVideoSource as a replacement for TVIVideoCapturer.");

/**
 *  @brief Developers shouldn't initialize this class directly.
 *
 *  @discussion Use `trackWithCapturer:` or `trackWithCapturer:enabled:constraints:name:` to create a `TVILocalVideoTrack`.
 */
- (null_unspecified instancetype)init __attribute__((unavailable("Use `trackWithCapturer:` or `trackWithCapturer:enabled:constraints:name:` to create a `TVILocalVideoTrack`.")));

/**
 *  @brief Creates a `TVILocalVideoTrack` with a `TVIVideoCapturer`.
 *
 *  @discussion This method allows you to provide a `TVIVideoCapturer`, and uses the default `TVIVideoConstraints`.
 *  @see [TVIVideoCapturer](TVIVideoCapturer.h)
 *
 *  @param capturer A `TVIVideoCapturer` which will provide the content for this Track.
 *
 *  @return A Track which is ready to be shared with Participants in a Room, or `nil` if an error occurs.
 */
+ (nullable instancetype)trackWithCapturer:(nonnull id<TVIVideoCapturer>)capturer
DEPRECATED_MSG_ATTRIBUTE("Use trackWithSource: and provide a TVIVideoSource instead.");

/**
 *  @brief Creates a `TVILocalVideoTrack` with a `TVIVideoCapturer`, `TVIVideoConstraints` and an enabled setting.
 *
 *  @discussion This method allows you to provide specific `TVIVideoConstraints`, and produce a disabled Track if you wish.
 *  @see [TVIVideoCapturer](TVIVideoCapturer.h)
 *
 *  @param capturer A `TVIVideoCapturer` which will provide the content for this Track.
 *  @param enabled Determines if the Track is enabled at creation time.
 *  @param constraints A `TVIVideoConstraints` which specifies requirements for video capture.
 *  @param name A name for the Track. Names are immutable and can only be provided at Track creation time.
 *
 *  @return A Track which is ready to be shared with Participants in a Room, or `nil` if an error occurs.
 */
+ (nullable instancetype)trackWithCapturer:(nonnull id<TVIVideoCapturer>)capturer
                                   enabled:(BOOL)enabled
                               constraints:(nullable TVIVideoConstraints *)constraints
                                      name:(nullable NSString *)name
DEPRECATED_MSG_ATTRIBUTE("Use trackWithSource:enabled:name: and provide a TVIVideoSource instead.");

/**
 *  @brief The video source that is associated with this track when using the `TVIVideoSource` based initializers.
 */
@property (nonatomic, strong, readonly, nullable) id<TVIVideoSource> source;

/**
 *  @brief Creates a `TVILocalVideoTrack` with a `TVIVideoSource`.
 *
 *  @discussion The Track will be enabled, and use a randomly generated name.
 *  @see [TVIVideoSource](TVIVideoSource.h)
 *
 *  @param source A `TVIVideoSource` which will provide the content for this Track.
 *
 *  @return A Track which is ready to be shared with Participants in a Room, or `nil` if an error occurs.
 */
+ (nullable instancetype)trackWithSource:(nonnull id<TVIVideoSource>)source;

/**
 *  @brief Creates a `TVILocalVideoTrack` with a `TVIVideoSource`.
 *
 *  @discussion The Track will be enabled, and use a randomly generated name.
 *  @see [TVIVideoSource](TVIVideoSource.h)
 *
 *  @param source A `TVIVideoSource` which will provide the content for this Track.
 *  @param enabled Determines if the Track is enabled at creation time.
 *  @param name A name for the Track. Names are immutable and can only be provided at Track creation time.
 *
 *  @return A Track which is ready to be shared with Participants in a Room, or `nil` if an error occurs.
 */
+ (nullable instancetype)trackWithSource:(nonnull id<TVIVideoSource>)source
                                 enabled:(BOOL)enabled
                                    name:(nullable NSString *)name;

@end
