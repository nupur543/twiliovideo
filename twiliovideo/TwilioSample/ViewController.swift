//
//  ViewController.swift
//  TwilioSample
//
//  Created by Nupur Sharma on 07/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit
import TwilioVideo

class ViewController: UIViewController {

    // Configure remote URL to fetch token from
    // Video SDK components
    var accessToken : String?
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }

    @IBAction func btnPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "checkCamSegue", sender: self)
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "checkCamSegue"{
            
            let destinatn = segue.destination as! CheckCameraController
            destinatn.accessToken = self.accessToken
        }
    }
}
