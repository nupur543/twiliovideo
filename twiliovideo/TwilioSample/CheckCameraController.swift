
import Foundation
import UIKit
import TwilioVideo
import CallKit

class CheckCameraController: UIViewController, TVICameraSourceDelegate{
    
    @IBOutlet weak var remoteMediaView: TVIVideoView!
    @IBOutlet weak var localMediaView: TVIVideoView!
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var btnDC: UIButton!
    
    var accessToken : String?
    var room: TVIRoom?
    var callUuid : UUID?
    
    var camera: TVICameraSource?
    var localVideoTrack: TVILocalVideoTrack?
    var localAudioTrack: TVILocalAudioTrack?
    var remoteParticipant: TVIRemoteParticipant?
    var remoteView: TVIVideoView?
    var audioDevice: TVIDefaultAudioDevice = TVIDefaultAudioDevice()
    
    var providerDelegate: CallProviderDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "QuickStart"
        
        TwilioVideo.audioDevice = self.audioDevice
        providerDelegate = CallProviderDelegate()
        
        if accessToken == nil{
            accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2UyNTJlYTIxNTVkYTI5N2VjMTM0NDMwODFmY2Q1NmI3LTE1NDc3MTY1MTQiLCJpc3MiOiJTS2UyNTJlYTIxNTVkYTI5N2VjMTM0NDMwODFmY2Q1NmI3Iiwic3ViIjoiQUNjYjI0ODUzN2QyN2Y3OTEzY2JlYWNiOTZlNWQ4MzQ2MCIsImV4cCI6MTU0NzcyMDExNCwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiVXNlciAxIiwidmlkZW8iOnsicm9vbSI6IlJhbmRvbSJ9fX0.g1GgcpMAZlVY_FSqi8K6Xb54m9TsSnaD1B_bSYNLRHE"
            
            self.startPreview { (error) in
                if error == nil{
                    self.connectToRoom(roomName: "Random")
                }
            }
        }else{
            //Call attend
           self.connectToRoom(roomName: "Random")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: Private
    func startPreview(completion: @escaping ((Error?) -> Void)) {
        
        let frontCamera = TVICameraSource.captureDevice(for: .front)
        let backCamera = TVICameraSource.captureDevice(for: .back)
        
        if (frontCamera != nil || backCamera != nil) {
            // Preview our local camera track in the local video preview view.
            camera = TVICameraSource(delegate: self)
            localVideoTrack = TVILocalVideoTrack.init(source: camera!, enabled: true, name: "Camera")
            
            // Add renderer to video track for local preview
          
            localVideoTrack!.addRenderer(localMediaView)
            print("Video track created")
            
            if (frontCamera != nil && backCamera != nil) {
                // We will flip camera on tap.
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.flipCamera))
                self.localMediaView.addGestureRecognizer(tap)
            }
            
            camera!.startCapture(with: frontCamera != nil ? frontCamera! : backCamera!) { (captureDevice, videoFormat, error) in
                if let error = error {
                    
                    print("Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    completion(error)
                } else {
                    self.localMediaView.shouldMirror = (captureDevice.position == .front)
                    completion(nil)
                    
                }
            }
        }
        else {
            print("No front or back capture device found!")
        }
    }
    
    func connectToRoom(roomName: String?){
        
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = TVIConnectOptions.init(token: accessToken!) { (builder) in
            
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [TVILocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [TVILocalVideoTrack]()
            
            // Use the preferred audio codec
            if let preferredAudioCodec = Settings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            
            // Use the preferred video codec
            if let preferredVideoCodec = Settings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            
            // Use the preferred encoding parameters
            if let encodingParameters = Settings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters
            }
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = "Random"
        }
        
        // Connect to the Room using the options we provided.
        room = TwilioVideo.connect(with: connectOptions, delegate: self)
    
    }
    
    @objc func flipCamera() {
        
        var newDevice: AVCaptureDevice?
        
        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = TVICameraSource.captureDevice(for: .back)
            } else {
                newDevice = TVICameraSource.captureDevice(for: .front)
            }
            
            if let newDevice = newDevice {
                camera.select(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        print("Error selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self.localMediaView.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
        }
    }
    
    func prepareLocalMedia() {
        
        // We will share local audio and video when we connect to the Room.
                // Create an audio track.
        if (localAudioTrack == nil) {
            localAudioTrack = TVILocalAudioTrack.init(options: nil, enabled: true, name: "Microphone")
            
            if (localAudioTrack == nil) {
                print("Failed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
        if (localVideoTrack == nil) {
            self.startPreview { (error) in
                self.connectToRoom(roomName: "Random")
            }
        }
    }
    
    func cleanupRemoteParticipant() {
        if ((self.remoteParticipant) != nil) {
            if ((self.remoteParticipant?.videoTracks.count)! > 0) {
                let remoteVideoTrack = self.remoteParticipant?.remoteVideoTracks[0].remoteTrack
                remoteVideoTrack?.removeRenderer(self.remoteMediaView!)
                self.remoteMediaView?.removeFromSuperview()
                self.remoteMediaView = nil
            }
        }
        self.remoteParticipant = nil
        if callUuid != nil{
              providerDelegate?.performEndCallAction(uuid: self.callUuid!)
        
        }
    }
    
    func holdCall(onHold: Bool) {
        localAudioTrack?.isEnabled = !onHold
        localVideoTrack?.isEnabled = !onHold
    }
    
    @IBAction func disconnectPressed(_ sender: UIButton) {
            self.room?.disconnect()
            myLabel.text = "\(room?.localParticipant!.identity) disconnected"
    }
    
}

extension CheckCameraController: TVIRoomDelegate{
 
    func didConnect(to room: TVIRoom) {
        
        // At the moment, this example only supports rendering one Participant at a time.
        
        print("Connected to room \(room.name) with \(room.remoteParticipants.count) participants")
        
        if (room.remoteParticipants.count > 0) {
            self.remoteParticipant = room.remoteParticipants[0]
            self.remoteParticipant?.delegate = self
        }
        
        myLabel.text = "\(room.localParticipant!.identity) Connected remote(\(room.remoteParticipants))"
        
    }
    
    func room(_ room: TVIRoom, didDisconnectWithError error: Error?) {
        print("Disconncted from room \(room.name), error = \(String(describing: error))")
        
        self.cleanupRemoteParticipant()
        self.room = nil
        
        if navigationController != nil{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.btnDC.removeFromSuperview()
            let localVideoTrack = room.localParticipant?.localVideoTracks[0].localTrack//self.remoteParticipant?.remoteVideoTracks[0].remoteTrack
            localVideoTrack?.removeRenderer(self.localMediaView!)
            camera?.stopCapture()
        }
    }
    
    func room(_ room: TVIRoom, didFailToConnectWithError error: Error) {
        print("Failed to connect to room with error \n \(error)")
        self.room = nil
    }
    
    func room(_ room: TVIRoom, participantDidConnect participant: TVIRemoteParticipant) {
       
        if (self.remoteParticipant == nil) {
            self.remoteParticipant = participant
            self.remoteParticipant?.delegate = self
        }
        print("Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
    }
    
    func room(_ room: TVIRoom, participantDidDisconnect participant: TVIRemoteParticipant) {
        if (self.remoteParticipant == participant) {
            cleanupRemoteParticipant()
        }
        print("Room \(room.name), Participant \(participant.identity) disconnected")
    }
}

extension CheckCameraController: TVIRemoteParticipantDelegate{
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        //Remote Participant has offered to share the video Track.
        print("Participant \(participant.identity) published \(publication.trackName) video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        // Remote Participant has stopped sharing the video Track.
        
        print("Participant \(participant.identity) unpublished \(publication.trackName) video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has offered to share the audio Track.
        
        print("Participant \(participant.identity) published \(publication.trackName) audio track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has stopped sharing the audio Track.
        
        print("Participant \(participant.identity) unpublished \(publication.trackName) audio track")
    }
    
    func subscribed(to videoTrack: TVIRemoteVideoTrack,
                    publication: TVIRemoteVideoTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's video frames now.
        
        print("Subscribed to \(publication.trackName) video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == participant) {
            // setupRemoteVideoView()
            videoTrack.addRenderer(self.remoteMediaView)
            myLabel.text = "Participant \(participant.identity) Connected"
            self.view.bringSubviewToFront(myLabel)
            self.view.bringSubviewToFront(btnDC)
        }
        
    }
    
    func unsubscribed(from videoTrack: TVIRemoteVideoTrack,
                      publication: TVIRemoteVideoTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        
        print("Unsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == participant) {
           self.cleanupRemoteParticipant()
        }
    }
    
    func subscribed(to audioTrack: TVIRemoteAudioTrack,
                    publication: TVIRemoteAudioTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        
        print("Subscribed to \(publication.trackName) audio track for Participant \(participant.identity)")
    }
    
    func unsubscribed(from audioTrack: TVIRemoteAudioTrack,
                      publication: TVIRemoteAudioTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        
        print("Unsubscribed from \(publication.trackName) audio track for Participant \(participant.identity)")
    }
   
}
