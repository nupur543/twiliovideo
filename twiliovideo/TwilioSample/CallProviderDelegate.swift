//
//  CallProviderDelegate.swift
//  TwilioSample
//  Created by Nupur Sharma on 16/01/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.

import UIKit
import CallKit
import AVFoundation

final class CallProviderDelegate: NSObject, CXProviderDelegate {
   
    private var provider: CXProvider? = nil
    var token : String?
    var callUuid : UUID?
    let callKitCallController: CXCallController
    
    override init() {
        provider = CXProvider(configuration: type(of: self).providerConfiguration)
        callKitCallController = CXCallController()
        super.init()
        
        provider!.setDelegate(self, queue: nil)
    }
    
    private func endOnGoingCall(){
        for call in CXCallObserver().calls {
            if call.hasEnded == false {
                self.performEndCallAction(uuid: call.uuid)
            }
        }
    }
    
    static var providerConfiguration: CXProviderConfiguration {
     
        let providerConfiguration = CXProviderConfiguration(localizedName: "TwilioSample")
        providerConfiguration.ringtoneSound = "ringtone.caf"
        providerConfiguration.supportsVideo = true
        providerConfiguration.supportedHandleTypes = [CXHandle.HandleType.generic]
        if let iconMaskImage = UIImage(named: "phone-symbol") {
            providerConfiguration.iconTemplateImageData = iconMaskImage.pngData()
        }
        
        return providerConfiguration
    }
    
    //Pass caller and call info to provider
    func reportIncomingCall( uuid: UUID,callUpdate: CXCallUpdate,accesstoken: String?,completion: ((NSError?) -> Void)? = nil) {
       
        provider!.reportNewIncomingCall(with: uuid, update: callUpdate) { error in

            self.token      = accesstoken
            self.callUuid   = uuid
            completion?(error as NSError?)
        }
    }
    
    func performStartCallAction(uuid: UUID, roomName: String?) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        
        startCallAction.isVideo = true
        
        let transaction = CXTransaction(action: startCallAction)
        
        callKitCallController.request(transaction)  { error in
            if let error = error {
                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            NSLog("StartCallAction transaction request successful")
        }
    }

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        /*
         1. Configure the audio session, but do not start to call audio here, since it must be done once the audio session has been activated by the system after having its priority elevated.
         2. Trigger the call to be answered via the underlying network service.
         3. Signal to the system that the action has been successfully performed.
         action.fulfill()
         
         4. Signal to the system that the action was unable to be performed.
         action.fail()
        */
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let videoViewController = storyBoard.instantiateViewController(withIdentifier: "CheckCameraController") as! CheckCameraController
        videoViewController.accessToken = self.token
        videoViewController.callUuid    = self.callUuid
        
        let state = UIApplication.shared.applicationState
        if state == .background {
            
            UIApplication.shared.keyWindow?.rootViewController?.present(videoViewController, animated: true, completion: nil)
        }else{
            
            let nav = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
            //print("nav \n \(nav)")
            if nav != nil {
               nav?.pushViewController(videoViewController, animated: true)
                
            }else{
               UIApplication.shared.keyWindow?.rootViewController?.present(videoViewController, animated: true, completion: nil)
            }
        }
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        /*
         1. Stop call audio whenever ending the call.
         2. Trigger the call to be ended via the underlying network service.
         3. Signal to the system that the action has been successfully performed.
         action.fulfill()
         4. Remove the ended call from the app's list of calls.
         */
    }
    
    func providerDidReset(_ provider: CXProvider) {
        
        self.endOnGoingCall()
    }
}

extension CallProviderDelegate{
    
    func performEndCallAction(uuid: UUID) {
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        
        callKitCallController.request(transaction) { error in
            if let error = error {
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                endCallAction.fail()
                return
            }
            endCallAction.fulfill()
            self.provider!.invalidate()
        }
    }
}

